package com.abm.apps.paymentservice.domain;

public enum PaymentStatus {
    PAYED,PENDING,CANCELLED;
}
