package com.abm.apps.im.constants.repository;

public interface MongoRepoConstants {
    String DOLLAR_ENDS_WITH = "$";
    String TRIANGLE_STARTS_WITH = "^";
    String IGNORE_CASE = "i";
    String LIKE_ANY = ".*";
}
