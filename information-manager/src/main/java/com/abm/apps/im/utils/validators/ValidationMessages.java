package com.abm.apps.im.utils.validators;

public interface ValidationMessages {
    String fieldMustBeNullInformationType = "Field [%s] must be null/empty for InformationType.%s";
    String fieldMustNotBeNullInformationType = "Field [%s] must be non-null/empty for InformationType.%s";
}
