package com.abm.apps.im.utils.validators.enums;

public enum OperationType {
    INSERT, UPDATE, DELETE;
}
