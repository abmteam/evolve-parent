package com.abm.apps.tools.flowtracer.logging.enums;

public enum LogLevel {
    DEBUG, INFO, ERROR;
}
