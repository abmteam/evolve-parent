package com.abm.apps.tools.flowtracer.pojo;

public interface FlowTrace {
    String getAppName();
    String getStepName();
    long getTimestamp();
}
