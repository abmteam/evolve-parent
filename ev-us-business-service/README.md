


## Description

Main Orchestrating component which receives calls from BFF for specific flow operations.
For each main flow operation, it will make the required calls to other microservices
in order to return the Main Operation Result
