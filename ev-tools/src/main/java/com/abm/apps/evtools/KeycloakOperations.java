package com.abm.apps.evtools;

public interface KeycloakOperations {


    String KEYCLOAK_URL = "";

    String KEYCLOAK_REALM = "";

    String BASE_URL = "/%s";

    String USERS_URL = BASE_URL + "/users";

}
