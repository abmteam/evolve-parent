package com.abm.apps.evtools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvToolsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvToolsApplication.class, args);
    }

}
