package com.abm.apps.evcommons.beans.domain.payment.product.periods;

public enum SubscriptionPeriodType {
    ONE_MONTH, SIX_MONTHS, ONE_YEAR, INFINITE;
}
