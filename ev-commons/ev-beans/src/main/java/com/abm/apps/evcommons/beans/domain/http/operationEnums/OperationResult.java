package com.abm.apps.evcommons.beans.domain.http.operationEnums;

public enum OperationResult {
    SUCCESS,
    FAILED,
    WARNING,
    ERROR;
}
