package com.abm.apps.evcommons.beans.domain.http.payloads.informationManager.filter.types;

public enum FieldTypes {
    NAME, DESCRIPTION, META;
}
