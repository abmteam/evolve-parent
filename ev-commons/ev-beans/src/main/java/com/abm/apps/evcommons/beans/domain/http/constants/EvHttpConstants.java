package com.abm.apps.evcommons.beans.domain.http.constants;

public interface EvHttpConstants {
    String USERNAME = "username";
    String INFORMATION_LIST_NAME = "listName";
    String INFORMATION_ID = "informationId";
    String INFORMATION_IDS = "informationIds";
    String INFORMATION_REF_NAME = "refName";
    String INFORMATION_REF_NAMES = "refNames";
}
