package com.abm.apps.evcommons.beans.domain.http.payloads.informationManager.filter.types;

public enum ValueFilterTypes {
    STARTS_WITH, CONTAINS, ENDS_WITH;
}
