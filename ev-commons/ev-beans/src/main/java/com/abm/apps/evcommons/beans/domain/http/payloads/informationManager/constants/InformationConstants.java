package com.abm.apps.evcommons.beans.domain.http.payloads.informationManager.constants;

public interface InformationConstants {
    String RELATION_LIST = "relationList";
    String TAG_LIST = "tagList";
}
