package com.abm.apps.evcommons.beans.domain.payment.product.currencies;

public enum Currency {
    EUR, USD, RON, POUND;
}
