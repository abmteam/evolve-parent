package com.abm.apps.evcommons.beans.domain.http;

public enum RequestResponseType {
    CREATE_USER,
    UPDATE_USER,
    GET_ACCOUNT_DETAILS,
    GET_CONTENT_BY_NODE,
    CREATE_SUBSCRIPTION,
    UPDATE_SUBSCRIPTION,
    MAKE_PAYMENT;
}
