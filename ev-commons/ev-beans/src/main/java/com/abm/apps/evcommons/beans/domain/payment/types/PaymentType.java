package com.abm.apps.evcommons.beans.domain.payment.types;

public enum PaymentType {
    CREDIT_CARD, PAYPAL;
}
