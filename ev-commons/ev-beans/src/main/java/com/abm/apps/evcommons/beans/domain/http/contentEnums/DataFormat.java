package com.abm.apps.evcommons.beans.domain.http.contentEnums;

/**
 * Specifies the type of the information hold by the record
 */
public enum DataFormat {
    TEXT, BINARY;
}
