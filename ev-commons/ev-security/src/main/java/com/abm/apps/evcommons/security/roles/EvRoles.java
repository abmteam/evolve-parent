package com.abm.apps.evcommons.security.roles;


public interface EvRoles {
    String ADMIN_ROLE = "ADMIN_ROLE";
    String USER_ROLE = "USER_ROLE";
}
