package com.abm.apps.evcommons.utils.config;

public interface InformationServiceCalls {
    String INFORMATION_MANAGER_GET_CONTENT_BY_LIST = "/getContentByListName";
    String INFORMATION_MANAGER_CREATE_INFORMATION = "/createInformation";
    String INFORMATION_MANAGER_UPDATE_INFORMATION = "/updateInformation";
    String INFORMATION_MANAGER_DELETE_INFORMATION_BY_ID = "/deleteInformationById";
    String INFORMATION_MANAGER_DELETE_INFORMATION_BY_REF = "/deleteInformationByRef";
    String INFORMATION_MANAGER_DELETE_INFORMATION_BY_IDS = "/deleteInformationByIds";
    String INFORMATION_MANAGER_GET_CONTENT_BY_REF = "/getContentByRef";
    String INFORMATION_MANAGER_GET_INFORMATION_BY_ID = "/getInformationById";
    String INFORMATION_MANAGER_GET_INFORMATIONS_BY_IDS = "/getInformationsByIds";
    String INFORMATION_MANAGER_GET_INFORMATION_BY_FILTER = "/getInformationByFilter";
    String INFORMATION_MANAGER_GET_INFORMATION_BY_REF = "/getInformationByRef";
    String INFORMATION_MANAGER_GET_INFORMATION_BY_REFS = "/getInformationsByRefs";

}
