package com.abm.apps.evcommons.utils.exceptions.constants;

public enum ExceptionTypes {
    VALIDATION, BUSINESS_FLOW;
}
