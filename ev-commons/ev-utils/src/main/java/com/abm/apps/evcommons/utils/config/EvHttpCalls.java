package com.abm.apps.evcommons.utils.config;

public interface EvHttpCalls extends InformationServiceCalls, PaymentServiceCalls, UserServiceCalls, BusinessServiceCalls {
}
