package com.abm.apps.evcommons.utils.config;

public interface PaymentServiceCalls {
    String PAYMENT_SERVICE_MAKE_PAYMENT = "/makePayment";
    String PAYMENT_SERVICE_GET_PAYMENT = "/getPayment";
}
