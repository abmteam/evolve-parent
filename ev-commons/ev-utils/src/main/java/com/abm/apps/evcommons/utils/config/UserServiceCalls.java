package com.abm.apps.evcommons.utils.config;

public interface UserServiceCalls {
    String USER_SERVICE_CREATE_USER = "/createUser";
    String USER_SERVICE_UPDATE_USER = "/updateUser";
    String USER_SERVICE_GET_ACCOUNT_DETAILS = "/getAccountDetails";
    String USER_SERVICE_CREATE_SUBSCRIPTION = "/createSubscription";
    String USER_SERVICE_CHANGE_SUBSCRIPTION = "/changeSubscription";
    String USER_SERVICE_AUTHENTICATE_EV_USER = "/authenticateEvUser";
}
