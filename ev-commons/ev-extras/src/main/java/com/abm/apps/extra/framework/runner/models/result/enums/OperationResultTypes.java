package com.abm.apps.extra.framework.runner.models.result.enums;

public enum OperationResultTypes {
    COMPLETION, EXCEPTION;
}
