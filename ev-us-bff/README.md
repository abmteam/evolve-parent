# T
## E
### S
#### T
##### I
###### N
###### G


## Description

BFF Component which is called by the Front-End to handle forwarding for the main operations.
This will forward the calls through the Gateway (Zuul) to each specific service.

In fact, it forwards the calls to Business Service which will do the necessary business checks and 
calls required to fulfill the required operation.